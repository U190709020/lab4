import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
		printBoard(board);
		while (true) {
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();

			while (true)
				if (row >= 1 && row <= 3 && col >= 1 && col <= 3 && board[row - 1][col - 1] == ' ') {
					break;
				} else {
					if (row > 3 || row < 1 || col > 3 || col < 1) {
						System.out.println("Please review your numbers");
						System.out.print("Player 1 enter row number:");
						row = reader.nextInt();
						System.out.print("Player 1 enter column number:");
						col = reader.nextInt();
					}
					if (board[row - 1][col - 1] != ' ') {
						System.out.println("Please check the blanks ");
						System.out.print("Player 1 enter row number:");
						row = reader.nextInt();
						System.out.print("Player 1 enter column number:");
						col = reader.nextInt();
					}
				}
			board[row - 1][col - 1] = 'X';
			printBoard(board);

			if (checkboard(board) == true) {
				System.out.println("Player 1 won!!");
				break;
			}

			if (board[0][0] != (' ') && board[1][1] != (' ') && board[2][2] != (' ') && board[0][1] != (' ') && board[0][2] != (' ') && board[1][0] != (' ') &&
					board[1][2] != (' ') && board[2][1] != (' ') && board[2][0] != (' ')) {
				break;
			}


			System.out.print("Player 2 enter row number:");
			row = reader.nextInt();
			System.out.print("Player 2 enter column number:");
			col = reader.nextInt();
			while (true)
				if (row >= 1 && row <= 3 && col >= 1 && col <= 3 && board[row - 1][col - 1] == ' ') {
					break;
				} else {
					if (row > 3 || row < 1 || col > 3 || col < 1) {
						System.out.println("Please review your numbers");
						System.out.print("Player 2 enter row number:");
						row = reader.nextInt();
						System.out.print("Player 2 enter column number:");
						col = reader.nextInt();
					}
					if (board[row - 1][col - 1] != ' ') {
						System.out.println("Please check the blanks ");
						System.out.print("Player 2 enter row number:");
						row = reader.nextInt();
						System.out.print("Player 2 enter column number:");
						col = reader.nextInt();
					}
				}
			board[row - 1][col - 1] = 'O';
			printBoard(board);
			if (checkboard(board) == true) {
				System.out.println("Player 2 won!!");
				break;
			}
		}
		if (checkboard(board) == false) {
			System.out.println("The game is a draw");
		}
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

	public static boolean checkboard(char[][] board) {
		if (board[0][2] != ' ' && board[0][2] == board[1][1] && board[0][2] == board[2][0])
			return true;
		else if (board[0][0] != ' ' && board[0][0] == board[1][1] && board[0][0] == board[2][2])
			return true;
		for (int i = 0; i < 3; i++) {
			if (board[i][0] != ' ' && board[i][0] == board[i][1] && board[i][0] == board[i][2])
				return true;
			else if (board[0][i] != ' ' && board[0][i] == board[1][i] && board[0][i] == board[2][i])
				return true;
		}
		return false;

	}
}